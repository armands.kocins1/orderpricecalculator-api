package com.valiit.pricecalculator.rest;

import com.valiit.pricecalculator.model.Order;
import com.valiit.pricecalculator.repository.PriceCalculatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
@CrossOrigin("*")
public class PriceCalculatorController {

    @Autowired
    private PriceCalculatorRepository priceCalculatorRepository;

    @GetMapping
    public List<Order> getOrders() {
        return priceCalculatorRepository.getOrders();
    }

    @PostMapping
    public void saveOrder(@RequestBody Order order) {
        priceCalculatorRepository.addOrder(order);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable("id") int id) {
        priceCalculatorRepository.deleteOrder(id);
    }

}