package com.valiit.pricecalculator.repository;



import com.valiit.pricecalculator.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class PriceCalculatorRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Order> getOrders() {
        return jdbcTemplate.query("select * from `order`", mapOrderRows);

    }


    public void addOrder(Order order) {
        jdbcTemplate.update(
                "insert into `order` (customer_name, customer_mail, customer_phone, customer_lang, service_type, quality_level, file_format, page_count, order_price) values (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                order.getCustomer_name(), order.getCustomer_mail(), order.getCustomer_phone(), order.getCustomer_lang(), order.getService_type(),
                order.getQuality_level(), order.getFile_format(), order.getPage_count(), order.getOrder_price()
        );
    }






    public void deleteOrder(int id) {
        jdbcTemplate.update("delete from `order` where id = ?", id);
    }



    private RowMapper<Order> mapOrderRows = (rs, rowNum) -> {
        Order order = new Order();
        order.setId(rs.getInt("id"));
        order.setCustomer_name(rs.getString("customer_name"));
        order.setCustomer_mail(rs.getString("customer_mail"));
        order.setCustomer_phone(rs.getString("customer_phone"));
        // != null ? rs.getDate("established").toLocalDate() : null);
        order.setCustomer_lang(rs.getString("customer_lang"));
        order.setService_type(rs.getString("service_type"));
        order.setQuality_level(rs.getString("quality_level"));
        order.setFile_format(rs.getString("file_format"));
        order.setPage_count(rs.getFloat("page_count"));
        order.setOrder_price(rs.getFloat("order_price"));
        return order;
    };

}
