package com.valiit.pricecalculator.model;

public class Order {


    private Integer id;
    private String customer_name;
    private String customer_mail;
    private String customer_phone;
    private String customer_lang;
    private String service_type;
    private String quality_level;
    private String file_format;
    private Float page_count;
    private Float order_price;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_mail() {
        return customer_mail;
    }

    public void setCustomer_mail(String customer_mail) {
        this.customer_mail = customer_mail;
    }

    public String getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(String customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getCustomer_lang() {
        return customer_lang;
    }

    public void setCustomer_lang(String customer_lang) {
        this.customer_lang = customer_lang;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getQuality_level() {
        return quality_level;
    }

    public void setQuality_level(String quality_level) {
        this.quality_level = quality_level;
    }

    public String getFile_format() {
        return file_format;
    }

    public void setFile_format(String file_format) {
        this.file_format = file_format;
    }

    public Float getPage_count() {
        return page_count;
    }

    public void setPage_count(Float page_count) {
        this.page_count = page_count;
    }

    public Float getOrder_price() {
        return order_price;
    }

    public void setOrder_price(Float order_price) {
        this.order_price = order_price;
    }
}
