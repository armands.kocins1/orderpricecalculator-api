-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pricecalculator.order
DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_mail` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_lang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quality_level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_format` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_extension` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_count` float DEFAULT NULL,
  `order_price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pricecalculator.order: ~34 rows (approximately)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `customer_name`, `customer_mail`, `customer_phone`, `customer_lang`, `service_type`, `quality_level`, `file_format`, `file_extension`, `page_count`, `order_price`) VALUES
	(1, 'Risto Kristo', 'risto.kristo@gmail.com', '+3725485458', 'EST', 'Translation', 'Attractive', 'DOC', NULL, 4, 23),
	(2, 'Kersti Varsti', 'kersti.varsti@tere.ee', '+3725787878', 'EST', 'DTP', 'Medium', 'PDF', NULL, 6, 46),
	(3, 'Inka Turunen', 'inka.turunen@sahko.fi', '+3584444444', 'FIN', 'Localization', 'Just for me', 'TXT', NULL, 8, 79),
	(4, 'Risto Kristo', 'risto.kristo@gmail.com', '+3725485458', 'EST', 'Translation', 'Medium', 'DOC', NULL, 4, 23),
	(6, 'Risto Kristo', 'risto.kristo@gmail.com', '', 'EST', 'Translation', NULL, 'DOC', NULL, 4, 23),
	(7, 'Risto Kristo', 'risto.kristo@gmail.com', '+3725485458', 'EST', 'Translation', NULL, 'DOC', NULL, 4, 23),
	(8, 'Risto Kristo', 'risto.kristo@gmail.com', '+3725485458', 'EST', 'Translation', 'High', 'DOC', NULL, 4, 23),
	(9, 'sdafdasf', 'sadfsdafsdf', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'Marek', 'marek@ttt.ee', '5678', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'sadf', 'asdfsda', 'asdfdasf', 'EN', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'ssAcv ewre', 'aaddg qjeo', '42973826', 'LV', NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'ssAcv ewre', 'aaddg qjeo', '42973826', 'LV', NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 'ssAcv ewre', 'aaddg qjeo', '42973826', 'LV', NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 'ssAcv ewre', 'aaddg qjeo', '42973826', 'LV', NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 'ssAcv ewre', 'aaddg qjeo', '42973826', 'LV', NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 'sdafds', 'asdfdsf', '23535345', 'EN', '1.2', NULL, NULL, NULL, NULL, NULL),
	(18, 'uuuuuuuuuuuuu', 'yyyyyyyyyyyy', '5655555555555555', 'LT', '1', '1.6', NULL, NULL, NULL, NULL),
	(19, 'uuuuuuuuuuuuu', 'yyyyyyyyyyyy', '5655555555555555', 'LT', '1', '1.6', NULL, NULL, NULL, NULL),
	(20, 'uuuuuuuuuuuuuu', 'nnnnnnnnnnnnnnn', '888888888888888', 'EN', '1', '1.6', '1.6', NULL, NULL, NULL),
	(21, 'rrrrrrrrrrrrrrr', 'yyyyyyyyyy', '66666666666666666', 'LT', '1', '1.6', '1.6', NULL, NULL, NULL),
	(22, 'rrrrrrrrrrrrrrr', 'yyyyyyyyyy', '66666666666666666', 'LT', '1', '1.6', '1.6', NULL, NULL, NULL),
	(23, '', '', '', 'EN', '1', '1.6', '1.6', NULL, 0, NULL),
	(24, 'frdtrrer', 'nnnnnnnnnnnnnnnn', '44444444', 'LT', '1', '1.6', '1.6', NULL, 3, NULL),
	(25, 'frdtrrer', 'nnnnnnnnnnnnnnnn', '44444444', 'LT', '1', '1.6', '1.6', NULL, 3, NULL),
	(26, 'frdtrrer', 'nnnnnnnnnnnnnnnn', '44444444', 'LT', '1', '1.6', '1.6', NULL, 3, NULL),
	(27, 'frdtrrer', 'nnnnnnnnnnnnnnnn', '44444444', 'LT', '1', '1.6', '1.6', NULL, 3, NULL),
	(28, 'frdtrrer', 'nnnnnnnnnnnnnnnn', '44444444', 'LT', '1', '1.6', '1.6', NULL, 3, NULL),
	(29, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1.6', NULL, 2626, 36301.8),
	(30, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(31, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(32, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(33, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(34, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(35, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(36, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(37, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(38, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(39, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(40, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6),
	(41, 'ghjhg', 'ghjghj', 'gjgh', 'EN', '1.2', '0.8', '1', NULL, 2626, 22688.6);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Dumping structure for table pricecalculator.translation
DROP TABLE IF EXISTS `translation`;
CREATE TABLE IF NOT EXISTS `translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `source_lang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_lang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_translation_order` (`order_id`),
  CONSTRAINT `FK_translation_order` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pricecalculator.translation: ~2 rows (approximately)
/*!40000 ALTER TABLE `translation` DISABLE KEYS */;
INSERT INTO `translation` (`id`, `order_id`, `source_lang`, `target_lang`) VALUES
	(1, 2, 'Danish', 'Estonian'),
	(2, 3, 'Latvian', 'English'),
	(3, 1, 'Estonian', 'Lithuanian');
/*!40000 ALTER TABLE `translation` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
